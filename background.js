const base = "zoom.us/wc/join/";
console.log("start of extension");

chrome.webRequest.onBeforeRequest.addListener(
  function(details) {
    console.log("start");
    return {
      redirectUrl:
        "https://" + details.url.match(/^https?:\/\/([\S\s]*)zoom.us/)[1] + base + details.url.match(/^https?:\/\/[^\/]+\/[*j]\/([\S\s]*)/)[1]
    };
  },
  {
    urls: [
      "*://*.zoom.us/.j/*",
      "*://*.zoom.us/j/*",
      "*://zoom.us/.j/*",
      "*://zoom.us/j/*",
    ],
    types: [
      "main_frame",
      "sub_frame",
      "stylesheet",
      "script",
      "image",
      "object",
      "xmlhttprequest",
      "other"
    ]
  },
  ["blocking"]
);
